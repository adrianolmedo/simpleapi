package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/peterbourgon/ff/v3"
)

func main() {
	fs := flag.NewFlagSet("simpleapi", flag.ExitOnError)
	var (
		port = fs.String("port", "80", "Internal container port.")
	)

	// Pass env vars to flags.
	ff.Parse(fs, os.Args[1:], ff.WithEnvVarNoPrefix())

	err := router().Listen(":" + *port)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}

// User model
type User struct {
	ID   int
	Name string
}

var users = []User{
	{
		ID:   1,
		Name: "John Doe",
	},
	{
		ID:   2,
		Name: "Adrian Olmedo",
	},
}

func router() *fiber.App {
	f := fiber.New()
	g := f.Group("/v1/users")
	g.Get("", listUsers())
	return f
}

// listUsers handler GET: /users
func listUsers() fiber.Handler {
	return func(c *fiber.Ctx) error {
		resp := respJSON(msgOK, "", users)
		return c.Status(http.StatusOK).JSON(resp)
	}
}
